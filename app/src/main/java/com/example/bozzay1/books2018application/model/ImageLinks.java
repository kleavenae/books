package com.example.bozzay1.books2018application.model;

import lombok.Data;

@Data
public class ImageLinks {
    private String smallThumbnail;
    private String thumbnail;
}
