package com.example.bozzay1.books2018application.api;

import com.example.bozzay1.books2018application.model.SearchResults;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface GoogleBooksService {
    @GET("/books/v1/volumes")
    Call<SearchResults> search(@Query("q") String query);
}
