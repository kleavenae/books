package com.example.bozzay1.books2018application.activity;

import android.databinding.DataBindingUtil;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import com.example.bozzay1.books2018application.R;
import com.example.bozzay1.books2018application.app.BookSearchApplication;
import com.example.bozzay1.books2018application.databinding.ActivityBookDetailsBinding;
import com.example.bozzay1.books2018application.model.BookDetails;

import butterknife.ButterKnife;
import butterknife.Unbinder;
import lombok.val;

public class BookDetailsActivity extends AppCompatActivity {

    public static final String BOOK_POSITION = "BookPosition";
    private int bookPosition;
    private BookDetails bookDetails = new BookDetails();

    private final Helpers HELPERS = new Helpers();

    private Unbinder unbinder;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        unbinder = ButterKnife.bind(this);

        val ab = getSupportActionBar();
        if (ab != null) {
            ab.hide();
        }

        ActivityBookDetailsBinding binding = DataBindingUtil.setContentView(this, R.layout.activity_book_details);
        bookPosition = getIntent().getIntExtra(BOOK_POSITION, 0);
        bookDetails.setVolumeInfo(HELPERS.getApplication().getBooks().get(bookPosition).getVolumeInfo());
        bookDetails.setIndex(bookPosition);
        binding.setBookDetails(bookDetails);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (unbinder != null) {
            unbinder.unbind();
        }
    }

    public void onShowPreviewBook(View view) {
        show(--bookPosition);
    }

    public void onShowNextBook(View view) {
        show(++bookPosition);
    }

    private void show(int position) {
        bookDetails.setIndex(position);
        bookDetails.setVolumeInfo(HELPERS.getApplication().getBooks().get(position).getVolumeInfo());
    }

    private class Helpers {
        BookSearchApplication getApplication() {
            return ((BookSearchApplication) BookDetailsActivity.this.getApplication());
        }
    }
}
