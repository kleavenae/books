package com.example.bozzay1.books2018application.adapter;

import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.bozzay1.books2018application.BR;
import com.example.bozzay1.books2018application.R;
import com.example.bozzay1.books2018application.model.Book;

import java.util.List;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;
import lombok.val;

@EqualsAndHashCode(callSuper = true)
@ToString
public class BooksRecyclerAdapter extends RecyclerView.Adapter<BooksRecyclerAdapter.BindingHolder> {

    private List<Book> books;
    public BooksRecyclerAdapter(List<Book> books) {
        this.books = books;
    }

    @NonNull
    @Override
    public BindingHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        val v = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_search_result, parent, false);
        return new BindingHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull BindingHolder holder, int position) {
        holder.getBinding().setVariable(BR.Book, books.get(position));
        holder.getBinding().executePendingBindings();
    }

    @Override
    public int getItemCount() {
        return this.books.size();
    }

    static class BindingHolder extends RecyclerView.ViewHolder {
        @Getter private ViewDataBinding binding;

        BindingHolder(View itemView) {
            super(itemView);
            binding = DataBindingUtil.bind(itemView);
        }
    }
}