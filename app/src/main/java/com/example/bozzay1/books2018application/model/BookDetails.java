package com.example.bozzay1.books2018application.model;

import android.databinding.BaseObservable;
import android.databinding.Bindable;
import com.example.bozzay1.books2018application.BR;
import lombok.*;

@EqualsAndHashCode(callSuper = true)
@ToString
public class BookDetails extends BaseObservable {
    private VolumeInfo volumeInfo;
    private int index;

    @Bindable
    public VolumeInfo getVolumeInfo() {
        return volumeInfo;
    }
    @Bindable
    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
        notifyPropertyChanged(BR.index);
    }
    public void setVolumeInfo(VolumeInfo volumeInfo) {
        this.volumeInfo = volumeInfo;
        notifyPropertyChanged(BR.volumeInfo);
    }
}
