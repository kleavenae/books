package com.example.bozzay1.books2018application.model;

import android.support.annotation.Nullable;

import java.util.List;

import lombok.Data;

@Data
public class VolumeInfo {
    private String title;
    private @Nullable String subtitle;
    private List<String> authors;
    private String publisher;
    private String description;
    private ImageLinks imageLinks;
}
