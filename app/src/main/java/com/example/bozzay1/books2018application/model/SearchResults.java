package com.example.bozzay1.books2018application.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

import lombok.Data;

@Data
public class SearchResults {
    private int totalItems;
    @SerializedName("items") private List<Book> books;
}
