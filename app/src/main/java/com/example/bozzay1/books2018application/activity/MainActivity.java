package com.example.bozzay1.books2018application.activity;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.example.bozzay1.books2018application.BuildConfig;
import com.example.bozzay1.books2018application.R;
import com.example.bozzay1.books2018application.adapter.BooksRecyclerAdapter;
import com.example.bozzay1.books2018application.api.GoogleBooksService;
import com.example.bozzay1.books2018application.app.BookSearchApplication;
import com.example.bozzay1.books2018application.databinding.ActivityMainBinding;
import com.example.bozzay1.books2018application.listener.RecyclerItemClickListener;
import com.example.bozzay1.books2018application.model.SearchResults;

import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import lombok.experimental.var;
import lombok.val;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static android.support.v7.widget.DividerItemDecoration.VERTICAL;
import static android.view.View.GONE;
import static android.view.View.VISIBLE;
import static okhttp3.logging.HttpLoggingInterceptor.Level.BODY;

public class MainActivity extends AppCompatActivity {

    private final Helpers HELPERS = new Helpers();

    @BindView(R.id.search_edit_text)                protected EditText searchEditText;
    @BindView(R.id.progress_bar)                    protected ProgressBar progressBar;
    @BindView(R.id.search_results_recycler_view)    protected RecyclerView searchResultRecyclerView;

    private GoogleBooksService googleBooksService;
    private BooksRecyclerAdapter booksRecyclerAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ActivityMainBinding binding = DataBindingUtil.setContentView(this, R.layout.activity_main);
        ButterKnife.bind(this);
        searchResultRecyclerView = findViewById(R.id.search_results_recycler_view);
        searchResultRecyclerView.addItemDecoration(new DividerItemDecoration(getApplicationContext(), VERTICAL));
        searchResultRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        searchResultRecyclerView.addOnItemTouchListener(new RecyclerItemClickListener(getApplicationContext(), (view, position) -> {
            Intent intent = new Intent(MainActivity.this, BookDetailsActivity.class);
            intent.putExtra(BookDetailsActivity.BOOK_POSITION, position);
            startActivity(intent);
        }));
        init();
    }

    private void init() {
        val retrofit = new Retrofit.Builder()
                .baseUrl("https://www.googleapis.com")
                .client(provideOkHttpClient())
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        googleBooksService = retrofit.create(GoogleBooksService.class);
    }

    private OkHttpClient provideOkHttpClient() {
        val builder = new OkHttpClient.Builder();
        if (BuildConfig.DEBUG) {
            val httpLoggingInterceptor = new HttpLoggingInterceptor();
            httpLoggingInterceptor.setLevel(BODY);
            builder.addInterceptor(httpLoggingInterceptor);
        }
        return builder.build();
    }

    @OnClick(R.id.search_button)
    public void onSearchButtonClicked() {
        val query = searchEditText.getText().toString().trim();
        if(TextUtils.isEmpty(query)) {
            searchEditText.setError(getString(R.string.required));
        }
        else {
            HELPERS.hideKeyboard();
            HELPERS.displayProgress(true);
            googleBooksService.search(query).enqueue(new Callback<SearchResults>() {
                @Override
                public void onResponse(@NonNull Call<SearchResults> call, @NonNull Response<SearchResults> response) {
                    HELPERS.displayResults(response.body());
                }

                @Override
                public void onFailure(@NonNull Call<SearchResults> call, @NonNull Throwable t) {
                    HELPERS.displayError(t);
                }
            });
        }
    }

    private class Helpers {
        BookSearchApplication getApplication() {
            return ((BookSearchApplication)MainActivity.this.getApplication());
        }
        void hideKeyboard() {
            val imm = (InputMethodManager)MainActivity.this.getSystemService(INPUT_METHOD_SERVICE);
            var view = MainActivity.this.getCurrentFocus();
            if (view != null) {
                view = new View(MainActivity.this);
            }
            Objects.requireNonNull(imm, "SoftInput is null").hideSoftInputFromWindow(Objects.requireNonNull(view).getWindowToken(), 0);
        }
        void displayProgress(boolean show) {
            searchResultRecyclerView.setVisibility(show?GONE:VISIBLE);
            progressBar.setVisibility(show?VISIBLE:GONE);
        }
        void displayResults(SearchResults results) {
            displayProgress(false);
            if (booksRecyclerAdapter == null) {
                booksRecyclerAdapter = new BooksRecyclerAdapter(results.getBooks());
                searchResultRecyclerView.setAdapter(booksRecyclerAdapter);
                getApplication().setBooks(results.getBooks());
            }
            else {
                getApplication().getBooks().clear();
                getApplication().getBooks().addAll(results.getBooks());
                booksRecyclerAdapter.notifyDataSetChanged();
            }
        }
        void displayError(Throwable t) {
            displayProgress(false);
            if (getApplication().getBooks() != null) {
                getApplication().getBooks().clear();
                if (booksRecyclerAdapter != null) {
                    booksRecyclerAdapter.notifyDataSetChanged();
                }
                Toast.makeText(MainActivity.this, getString(R.string.msg_error_generic), Toast.LENGTH_LONG).show();
            }
        }
    }
}
