package com.example.bozzay1.books2018application.model;

import lombok.Data;

@Data
public class Book {
    private VolumeInfo volumeInfo;
}
