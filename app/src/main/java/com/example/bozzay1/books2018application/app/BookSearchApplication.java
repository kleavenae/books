package com.example.bozzay1.books2018application.app;

import android.app.Application;
import com.example.bozzay1.books2018application.model.Book;
import java.util.List;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class BookSearchApplication extends Application {
    private List<Book> books;

    @Override
    public void onCreate() {
        super.onCreate();
    }
}
