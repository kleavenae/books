package com.example.bozzay1.books2018application.adapter;

import android.databinding.BindingAdapter;
import android.view.View;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

import lombok.experimental.UtilityClass;
import lombok.val;

import static android.view.View.*;

@UtilityClass
public class CustomBindingAdapter {
    @BindingAdapter({"imageUrl"})
    public static void loadImage(ImageView imageView, String url) {
        val picasso = new Picasso.Builder(imageView.getContext()).build();
        picasso.load(url).into(imageView);
    }
    @BindingAdapter({"visibilityOnIndex"})
    public static void toggleVisibility(View view, int index) {
        view.setVisibility(index > 0 ? VISIBLE : GONE);
    }
}
